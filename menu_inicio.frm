VERSION 5.00
Begin VB.Form Form2 
   BackColor       =   &H00400000&
   Caption         =   "Space Invaders -  Men�"
   ClientHeight    =   4464
   ClientLeft      =   3960
   ClientTop       =   2880
   ClientWidth     =   5904
   Icon            =   "menu_inicio.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   4464
   ScaleWidth      =   5904
   Begin VB.Frame Frame1 
      BackColor       =   &H00400000&
      Caption         =   "  Menu de inicio  "
      BeginProperty Font 
         Name            =   "Nasalization"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   4212
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5652
      Begin VB.CommandButton SALIR 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "Nasalization"
            Size            =   10.2
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   3240
         TabIndex        =   7
         Top             =   2760
         Width           =   1572
      End
      Begin VB.CommandButton CONTROLES 
         Caption         =   "Controles"
         BeginProperty Font 
            Name            =   "Nasalization"
            Size            =   10.2
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   600
         TabIndex        =   6
         Top             =   1320
         Width           =   1572
      End
      Begin VB.CommandButton INICIO 
         Caption         =   "Inicio"
         BeginProperty Font 
            Name            =   "Nasalization"
            Size            =   10.2
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   600
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   600
         Width           =   1572
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "= 40 ptos"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9.6
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   252
         Left            =   1080
         TabIndex        =   5
         Top             =   3600
         Width           =   1332
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "= 20 ptos"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9.6
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   252
         Left            =   960
         TabIndex        =   4
         Top             =   3000
         Width           =   1452
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "= 10 ptos"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9.6
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   252
         Left            =   960
         TabIndex        =   3
         Top             =   2400
         Width           =   1452
      End
      Begin VB.Image Image4 
         Height          =   504
         Left            =   240
         Picture         =   "menu_inicio.frx":27852
         Stretch         =   -1  'True
         Top             =   3480
         Width           =   696
      End
      Begin VB.Image Image3 
         Height          =   504
         Left            =   240
         Picture         =   "menu_inicio.frx":27A54
         Stretch         =   -1  'True
         Top             =   2880
         Width           =   696
      End
      Begin VB.Image Image2 
         Height          =   504
         Left            =   240
         Picture         =   "menu_inicio.frx":27CF6
         Stretch         =   -1  'True
         Top             =   2280
         Width           =   696
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Por: Johanna A. C."
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   10.2
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   252
         Left            =   3480
         TabIndex        =   2
         ToolTipText     =   "Para: Anderson E. A."
         Top             =   3720
         Width           =   1812
      End
      Begin VB.Image Image1 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   1920
         Left            =   2760
         Picture         =   "menu_inicio.frx":27F70
         Stretch         =   -1  'True
         Top             =   360
         Width           =   2520
      End
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub SALIR_Click()
    Unload Me
End Sub

Private Sub CONTROLES_Click()
    Form3.Show
End Sub

Public Sub INICIO_Click()
    Form1.Show
    Unload Me
End Sub

