VERSION 5.00
Begin VB.Form Form3 
   BackColor       =   &H00400000&
   Caption         =   "Space Invaders - Controles"
   ClientHeight    =   2724
   ClientLeft      =   108
   ClientTop       =   456
   ClientWidth     =   4500
   Icon            =   "controles.frx":0000
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   ScaleHeight     =   2724
   ScaleWidth      =   4500
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00400000&
      Caption         =   "  Controles  "
      BeginProperty Font 
         Name            =   "Nasalization"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   2412
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4212
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "  P             M             ->            <-    ESPACIO"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9.6
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   2292
         Left            =   3000
         TabIndex        =   2
         Top             =   360
         Width           =   1092
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Pausar                    Regresar al menu           Derecha                  Izquierda                 Disparar"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9.6
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   2292
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   2412
      End
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
