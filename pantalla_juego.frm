VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Space Invader - En curso"
   ClientHeight    =   8652
   ClientLeft      =   108
   ClientTop       =   456
   ClientWidth     =   14280
   Icon            =   "pantalla_juego.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   Picture         =   "pantalla_juego.frx":27852
   ScaleHeight     =   8652
   ScaleWidth      =   14280
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame Frame1 
      BackColor       =   &H00000000&
      Caption         =   " Puntos "
      BeginProperty Font 
         Name            =   "Nasalization"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   612
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   1332
      Begin VB.Label ptos 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9.6
            Charset         =   255
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   252
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   852
      End
   End
   Begin VB.Timer Timer2 
      Interval        =   1000
      Left            =   9720
      Top             =   8280
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   10320
      Top             =   8280
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   39
      Left            =   9240
      Picture         =   "pantalla_juego.frx":A0F3E
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   38
      Left            =   8400
      Picture         =   "pantalla_juego.frx":A11E0
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   37
      Left            =   7560
      Picture         =   "pantalla_juego.frx":A1482
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   36
      Left            =   6720
      Picture         =   "pantalla_juego.frx":A1724
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   35
      Left            =   5880
      Picture         =   "pantalla_juego.frx":A19C6
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   34
      Left            =   3720
      Picture         =   "pantalla_juego.frx":A1C68
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   33
      Left            =   2880
      Picture         =   "pantalla_juego.frx":A1F0A
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   32
      Left            =   2040
      Picture         =   "pantalla_juego.frx":A21AC
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   31
      Left            =   1200
      Picture         =   "pantalla_juego.frx":A244E
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   30
      Left            =   360
      Picture         =   "pantalla_juego.frx":A26F0
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   29
      Left            =   9240
      Picture         =   "pantalla_juego.frx":A2992
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   28
      Left            =   8400
      Picture         =   "pantalla_juego.frx":A2C0C
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   27
      Left            =   7560
      Picture         =   "pantalla_juego.frx":A2E86
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   26
      Left            =   6720
      Picture         =   "pantalla_juego.frx":A3100
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   25
      Left            =   5880
      Picture         =   "pantalla_juego.frx":A337A
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   24
      Left            =   3720
      Picture         =   "pantalla_juego.frx":A35F4
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   23
      Left            =   2880
      Picture         =   "pantalla_juego.frx":A386E
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   22
      Left            =   2040
      Picture         =   "pantalla_juego.frx":A3AE8
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   21
      Left            =   1200
      Picture         =   "pantalla_juego.frx":A3D62
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   20
      Left            =   360
      Picture         =   "pantalla_juego.frx":A3FDC
      Stretch         =   -1  'True
      Top             =   2400
      Width           =   696
   End
   Begin VB.Label contador 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "5"
      BeginProperty Font 
         Name            =   "Nasalization"
         Size            =   40.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   972
      Left            =   6840
      TabIndex        =   2
      Top             =   5040
      Width           =   972
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   19
      Left            =   9240
      Picture         =   "pantalla_juego.frx":A4256
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   18
      Left            =   8400
      Picture         =   "pantalla_juego.frx":A44D0
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   17
      Left            =   7560
      Picture         =   "pantalla_juego.frx":A474A
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   16
      Left            =   6720
      Picture         =   "pantalla_juego.frx":A49C4
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   15
      Left            =   5880
      Picture         =   "pantalla_juego.frx":A4C3E
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   14
      Left            =   3720
      Picture         =   "pantalla_juego.frx":A4EB8
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   13
      Left            =   2880
      Picture         =   "pantalla_juego.frx":A5132
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   12
      Left            =   2040
      Picture         =   "pantalla_juego.frx":A53AC
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   11
      Left            =   1200
      Picture         =   "pantalla_juego.frx":A5626
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   10
      Left            =   360
      Picture         =   "pantalla_juego.frx":A58A0
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   9
      Left            =   9240
      Picture         =   "pantalla_juego.frx":A5B1A
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   8
      Left            =   8400
      Picture         =   "pantalla_juego.frx":A5D1C
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   7
      Left            =   7560
      Picture         =   "pantalla_juego.frx":A5F1E
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   6
      Left            =   6720
      Picture         =   "pantalla_juego.frx":A6120
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   5
      Left            =   5880
      Picture         =   "pantalla_juego.frx":A6322
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Shape LASER 
      BorderColor     =   &H000080FF&
      FillColor       =   &H0000FFFF&
      FillStyle       =   0  'Solid
      Height          =   372
      Left            =   7320
      Shape           =   2  'Oval
      Top             =   7080
      Visible         =   0   'False
      Width           =   132
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   4
      Left            =   3720
      Picture         =   "pantalla_juego.frx":A6524
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   3
      Left            =   2880
      Picture         =   "pantalla_juego.frx":A6726
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   2
      Left            =   2040
      Picture         =   "pantalla_juego.frx":A6928
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   1
      Left            =   1200
      Picture         =   "pantalla_juego.frx":A6B2A
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "posicion NAVE"
      BeginProperty Font 
         Name            =   "Terminal"
         Size            =   9.6
         Charset         =   255
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF8080&
      Height          =   612
      Left            =   12600
      TabIndex        =   1
      Top             =   8040
      Visible         =   0   'False
      Width           =   1452
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "codigo de tecla"
      BeginProperty Font 
         Name            =   "Terminal"
         Size            =   9.6
         Charset         =   255
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF8080&
      Height          =   612
      Left            =   10920
      TabIndex        =   0
      Top             =   8040
      Visible         =   0   'False
      Width           =   1452
   End
   Begin VB.Image NAVE 
      Height          =   852
      Left            =   6960
      Picture         =   "pantalla_juego.frx":A6D2C
      Stretch         =   -1  'True
      Top             =   7440
      Width           =   852
   End
   Begin VB.Image Image1 
      Height          =   504
      Index           =   0
      Left            =   360
      Picture         =   "pantalla_juego.frx":A7016
      Stretch         =   -1  'True
      Top             =   960
      Width           =   696
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ed As Integer 'enemigos derrotados
Dim dir As Boolean 'direccion enemigos
Dim mov As Integer

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Label1 = KeyCode
    Label2 = NAVE.Left
    
    'MOVIMIENTOS JUGADOR
    Select Case KeyCode
        Case 37 'izquierda
            If (NAVE.Left > 500 And Timer1.Enabled = True) Then
                NAVE.Left = NAVE.Left - 200
            End If
        
        Case 39 'derecha
            If (NAVE.Left < 12900 And Timer1.Enabled = True) Then
                NAVE.Left = NAVE.Left + 200
            End If
        
        Case 32 'disparo
            If (LASER.Visible = False And Timer1.Enabled = True) Then
                LASER.Top = NAVE.Top - 200
                LASER.Left = NAVE.Left + 360
                LASER.Visible = True
            End If
            
        Case 80 'pause
            If (Timer1.Enabled = True And Timer2.Enabled = False) Then
                Timer1.Enabled = False
                Form1.Caption = "Space Invader - Pausado"
            ElseIf (Timer2.Enabled = False) Then
                Timer1.Enabled = True
                Form1.Caption = "Space Invader - En curso"
            End If
            
        Case 77 'volver al menu
            Form2.Show
            Unload Me
    End Select
End Sub

Private Sub Timer1_Timer()
        mov = mov + 1
        
        For n = 0 To Image1.UBound
        
            Select Case (dir) 'MOVIMIENTOS ENEMIGOS
                Case True 'derecha
                    Image1(n).Left = Image1(n).Left + 150
                Case False 'izquierda
                    Image1(n).Left = Image1(n).Left - 150
            End Select
            
            If (LASER.Visible = True And Image1(n).Visible = True) Then 'SI ENEMIGOS RECIBEN DISPARO
                If (LASER.Left >= Image1(n).Left And LASER.Left <= Image1(n).Left + 480) And (LASER.Top >= Image1(n).Top And LASER.Top <= Image1(n).Top + 720) Then
                    LASER.Visible = False
                    Image1(n).Visible = False
                    ed = ed + 1
                    If (n <= 9) Then
                        ptos.Caption = ptos.Caption + 40 'ptos enemigo morado
                    ElseIf (n >= 30) Then
                        ptos.Caption = ptos.Caption + 10 'ptos enemigo rosa
                    Else
                        ptos.Caption = ptos.Caption + 20 'ptos enemigo verde
                    End If
                End If
            End If
            
            If (Image1(n).Top > 6800 And Image1(n).Visible = True) Then 'GAME OVER - lose
                Timer1.Enabled = False
                MsgBox "Los extraterrestres han llegado al planeta, perdiste :(", vbDefaultButton1, "GAME OVER"
            End If
        Next
        
        If (mov = 26) Then 'LIMITE DE PANTALLA
            For m = 0 To Image1.UBound
                Image1(m).Top = Image1(m).Top + 150 'abajo
            Next
            If (dir = True) Then
                dir = False 'izquierda
                mov = 0
            Else
                dir = True 'derecha
                mov = 0
            End If
        End If
        
        If (LASER.Visible = True) Then 'MOVIMIENTO DEL DISPARO
            LASER.Top = LASER.Top - 400
            If (LASER.Top < 0) Then
                LASER.Visible = False
            End If
        End If
        
        If (ed = Image1.Count) Then 'GAME OVER - win
            Timer1.Enabled = False
            MsgBox "Has vencido a la horda extraterreste ¡ganaste! :D", vbDefaultButton1, "GAME OVER"
        End If
End Sub

Private Sub Timer2_Timer() 'CONTADOR DE INICIO
    If (contador.Caption = 0) Then
        dir = True
        Timer2.Enabled = False
        Timer1.Enabled = True
        contador.Visible = False
    End If
    contador.Caption = contador.Caption - 1
End Sub
